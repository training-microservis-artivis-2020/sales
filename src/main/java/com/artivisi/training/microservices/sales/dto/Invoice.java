package com.artivisi.training.microservices.sales.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Invoice {
    private String salesReference;
    private LocalDateTime transactionTime = LocalDateTime.now();
    private String customerName;
    private String customerEmail;
    private String description;
    private BigDecimal amount;
}
