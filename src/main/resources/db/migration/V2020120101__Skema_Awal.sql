create table sales (
    id varchar(36),
    transaction_time timestamp not null,
    customer_name varchar(100) not null,
    customer_email varchar(100) not null,
    product varchar(100) not null,
    quantity integer not null,
    unit_price decimal(19,2) not null,
    primary key (id)
);
